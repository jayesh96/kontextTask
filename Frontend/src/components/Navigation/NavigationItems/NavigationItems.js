import React from 'react';

import classes from './NavigationItems.css';
import NavigationItem from './NavigationItem/NavigationItem';

const navigationItems = () => (
    <ul className={classes.NavigationItems}>
        <NavigationItem link="/" active>Home</NavigationItem>
        <NavigationItem link="/">Heat Map</NavigationItem>
        <NavigationItem link="/">Frequent Tweets</NavigationItem>

    </ul>
);

export default navigationItems;