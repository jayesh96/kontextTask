import React, { Component } from 'react';

import Layout from './hoc/Layout/Layout';
import Tweets from './components/Tweets/Tweets'
import Map from './components/Map/Map';

class App extends Component {
  render () {
    return (
      <div>
        <Layout>
        <Tweets/>
        <Map/>
        </Layout>
      </div>
    );
  }
}

export default App;
