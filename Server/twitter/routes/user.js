var express = require('express');
var router = express.Router();
const User = require('../models/User');


router.route('/signup')
.get((req,res,next)=>{
    res.render('accounts/signup',{message:req.flash('errors')});
})
.post((req,res,next)=>{
    User.findOne({email:req.body.email}, function(err,existingUser){
        if(existingUser){
            console.log("hello1")
            res.json("This User is Already Registered!")
        }else{
            var user = new User();
            user.name = req.body.name;
            user.email = req.body.email;
            user.photo = user.gravatar();
            user.password = req.body.password;
            user.save((err)=>{
                //req.login(user, function(err){
                    if(err) return next(err);
                    res.redirect('/');
               // });
            });
        }
    });
});

module.exports = router;