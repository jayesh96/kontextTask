const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt-nodejs');
const crypto = require('crypto');

const TweetSchema = new Schema({
    text:{type:String,lowercase:true}
    , active     : Boolean
    , origin : String
});


TweetSchema.statics.getTweets = function(page, skip, callback) {

    var tweets = [],
        start = (page * 10) + (skip * 1);
  
    // Query the db, using skip and limit to achieve page chunks
    Tweet.find({},'text active origin',{skip: start, limit: 10}).sort({date: 'desc'}).exec(function(err,docs){
  
      // If everything is cool...
      if(!err) {
        tweets = docs;  // We got tweets
        console.log(tweets)
        tweets.forEach(function(tweet){
          tweet.active = true; // Set them to active
        });
      }
  
      // Pass them back to the specified callback
      callback(tweets);
  
    });
  
  };

  
module.exports = mongoose.model('Tweet',TweetSchema);