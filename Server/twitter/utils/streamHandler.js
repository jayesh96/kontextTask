var Tweet = require('../models/tweet');

module.exports = function(stream, io){

  console.log("hello")
  // When tweets get sent our way ...
  stream.on('data', function(data) {
    
    if (data['user'] !== undefined) {

      // Construct a new tweet object
      var tweet = {
        active: false,        
        text: data['text']['location'],
        origin: data['user'],
      };

      // Create a new model instance with our object
      var tweetEntry = new Tweet(tweet);

      // Save 'er to the database
      tweetEntry.save(function(err) {
        if (!err) {
          // If everything is cool, socket.io emits the tweet.
          io.emit('tweet', tweet);
        }
      });

    }

  });

};
