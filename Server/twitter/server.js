const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const hbs = require('hbs');
const expressHbs = require('express-handlebars');
const path = require('path');
const config = require('./config/config');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);
const flash = require('express-flash');
const passport = require('passport');
var Twit = require('twit')

var T = new Twit({
  consumer_key:         config.consumer_key,
  consumer_secret:      config.consumer_secret,
  access_token:         config.access_token,
  access_token_secret:  config.access_token_secret,
  timeout_ms:           60*1000,  // optional HTTP request timeout to apply to all requests.
  strictSSL:            true,     // optional - requires SSL certificates to be valid.
})


const app = express()
const http = require('http').Server(app);
const io = require('socket.io')(http);

mongoose.connect(config.database, { useNewUrlParser: true },function(err){
    if(err) console.log(err);
    console.log("Connected to server");
})

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(express.static(path.join(__dirname, 'public')));
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(session({
    resave:true,
    saveUninitialized:true,
    secret:config.secret,
    store:new MongoStore({ url:config.database,autoReconnect:true})
}));
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());


require('./realtime/io')(io);


const mainRoutes = require('./routes/main')
const userRoutes = require('./routes/user')
var Tweet = require('./models/tweet');

app.use('/',mainRoutes);
app.use('/user',userRoutes);


app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});




var stream = T.stream('statuses/filter', { track: 'Cloud' })
var i = 0;
stream.on('tweet', function (data) {
    console.log(data)
    if (data['user'] !== undefined) {

        // Construct a new tweet object
        var tweet = {
          active: false,        
          text: data['text'],
          origin: data['user']['location'],
        };
  
        // Create a new model instance with our object
        var tweetEntry = new Tweet(tweet);
        console.log("Passed!")
        // Save 'er to the database
        tweetEntry.save(function(err) {
          if (!err) {
            // If everything is cool, socket.io emits the tweet.
            io.emit('tweet', tweet);
          }else{
              console.log(err)
          }
        });
  
      }
  
})



http.listen(3030,(err) =>{
    if(err) console.log(err);
    console.log(`Running on port ${3030}`)
});